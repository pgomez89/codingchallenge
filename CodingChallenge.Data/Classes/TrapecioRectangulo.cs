﻿using System;

namespace CodingChallenge.Data.Classes
{
    public class TrapecioRectangulo: FormaGeometrica
    {
        protected readonly decimal _ladosup;
        protected readonly decimal _altura;
        protected readonly decimal _ladodiagonal;
        public static int cantidad;
        public static decimal area;
        public static decimal perimetro;

        public TrapecioRectangulo(decimal ladoinf, decimal ladosup, decimal altura) : base(ladoinf)
        {
            _ladosup = ladosup;
            _altura = altura;

            // Aplico teorema de pitágoras para obtener el lado faltante que se guarda en _ladodiagonal
            decimal lado1 = Math.Abs(_lado - _ladosup);
            decimal lado1alcuadrado = lado1 * lado1;
            decimal lado2alcuadrado = _altura * altura;
            decimal sumadelados = lado1alcuadrado + lado2alcuadrado;
            _ladodiagonal = (decimal)Math.Sqrt((double)sumadelados);

            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }

        public override decimal CalcularArea()
        {
            return (_lado + _ladosup) / 2 * _altura;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado + _ladosup + _altura + _ladodiagonal;
        }

        public override void RealizarCalculos()
        {
            cantidad++;
            area += CalcularArea();
            perimetro += CalcularPerimetro();
        }

        public static int GetCantidad()
        {
            return cantidad;
        }

        public static decimal GetArea()
        {
            return area;
        }

        public static decimal GetPerimetro()
        {
            return perimetro;
        }

        public static void BorrarDatos()
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }
    }
}
