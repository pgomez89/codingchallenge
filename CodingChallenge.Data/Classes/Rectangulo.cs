﻿namespace CodingChallenge.Data.Classes
{
    public class Rectangulo: FormaGeometrica
    {
        protected readonly decimal _lado2;
        public static int cantidad;
        public static decimal area;
        public static decimal perimetro;

        public Rectangulo(decimal ladobase, decimal ladoaltura) : base(ladobase)
        {
            _lado2 = ladoaltura;
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }

        public override decimal CalcularArea()
        {
            return _lado * _lado2;
        }

        public override decimal CalcularPerimetro()
        {
            return (2 * _lado) + (2 * _lado2);
        }

        public override void RealizarCalculos()
        {
            cantidad++;
            area += CalcularArea();
            perimetro += CalcularPerimetro();
        }

        public static int GetCantidad()
        {
            return cantidad;
        }

        public static decimal GetArea()
        {
            return area;
        }

        public static decimal GetPerimetro()
        {
            return perimetro;
        }

        public static void BorrarDatos()
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }
    }
}
