﻿using System;

namespace CodingChallenge.Data.Classes
{
    public class Circulo:FormaGeometrica
    {
        public static int cantidad;
        public static decimal area;
        public static decimal perimetro;

        public Circulo(decimal lado) : base(lado)
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (_lado / 2) * (_lado / 2);
        }

        public override decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * _lado;
        }

        public override void RealizarCalculos()
        {
            cantidad++;
            area += CalcularArea();
            perimetro += CalcularPerimetro();
        }

        public static int GetCantidad()
        {
            return cantidad;
        }

        public static decimal GetArea()
        {
            return area;
        }

        public static decimal GetPerimetro()
        {
            return perimetro;
        }

        public static void BorrarDatos()
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }
    }
}
