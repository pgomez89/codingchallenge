﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CodingChallenge.Data.Classes
{
    public class Reporte
    {
        private static int cantidad;
        private static decimal area;
        private static decimal perimetro;

        public static string Imprimir(List<FormaGeometrica> formas, int idioma)
        {
            // Si llegan a existir datos viejos en las formas, se borran. Se evita una posible inconsistencia en el reporte
            BorrarDatosExistentes();

            var sb = new StringBuilder();

            if (!formas.Any())
            {
                // No hay ninguna forma
                sb.Append(Idioma.ListaVacia(idioma));
            }
            else
            {
                // Hay por lo menos una forma

                // HEADER
                sb.Append(Idioma.Header(idioma));

                // Se realizan los cálculos de área y perímetro para cada forma, y se guarda su cantidad
                RealizarCalculos(formas);

                cantidad = 0;
                area = 0m;
                perimetro = 0m;
                // Se obtienen los resultados del cálculo de cada forma, y se agregan en el informe
                ObtenerResultados(sb, idioma);

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(cantidad + " " + Idioma.TraducirFormaTotal(idioma) + " ");
                sb.Append(Idioma.TraducirPerimetro(idioma) + " " + (perimetro).ToString("#.##") + " ");
                sb.Append(Idioma.TraducirArea(idioma) + " " + (area).ToString("#.##"));
            }

            return sb.ToString();
        }

        private static void BorrarDatosExistentes()
        {
            // Se obtienen todas las clases que heredan de la clase padre "FormaGeometrica"
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(FormaGeometrica)) && !t.IsAbstract);
            // Se borran los datos, en caso de existir, en cada forma geométrica
            foreach (var type in types)
            {
                type.GetMethod("BorrarDatos").Invoke(null, null);
            }
        }

        private static void RealizarCalculos(List<FormaGeometrica> formas)
        {
            for (var i = 0; i < formas.Count; i++)
            {
                formas[i].RealizarCalculos();
            }
        }

        private static void ObtenerResultados(StringBuilder sb, int idioma)
        {
            // Se obtienen todas las clases que heredan de la clase padre "FormaGeometrica"
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(FormaGeometrica)) && !t.IsAbstract);
            int c;
            decimal a, p;
            // Se obtiene cantidad, área y perímetro de cada forma geométrica. Se agrea la información en el reporte
            foreach (var type in types)
            {
                int.TryParse(type.GetMethod("GetCantidad").Invoke(null, null).ToString(), out c);
                decimal.TryParse(type.GetMethod("GetArea").Invoke(null, null).ToString(), out a);
                decimal.TryParse(type.GetMethod("GetPerimetro").Invoke(null, null).ToString(), out p);
                sb.Append(ObtenerLinea(c, a, p, type, idioma));
                cantidad += c;
                area += a;
                perimetro += p;
            }
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, Type type, int idioma)
        {
            if (cantidad > 0)
            {
                return $"{cantidad} {Idioma.TraducirForma(idioma, cantidad, type.Name)} | {Idioma.TraducirArea(idioma)} {area:#.##} | {Idioma.TraducirPerimetro(idioma)} {perimetro:#.##} <br/>";
            }

            return string.Empty;
        }
    }
}
