﻿namespace CodingChallenge.Data.Classes
{
    public class Cuadrado:FormaGeometrica
    {
        public static int cantidad;
        public static decimal area;
        public static decimal perimetro;

        public Cuadrado(decimal lado) : base(lado)
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }

        public override decimal CalcularArea()
        {
            return _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 4;
        }

        public override void RealizarCalculos()
        {
            cantidad++;
            area += CalcularArea();
            perimetro += CalcularPerimetro();
        }

        public static int GetCantidad()
        {
            return cantidad;
        }

        public static decimal GetArea()
        {
            return area;
        }

        public static decimal GetPerimetro()
        {
            return perimetro;
        }

        public static void BorrarDatos()
        {
            cantidad = 0;
            area = 0m;
            perimetro = 0m;
        }
    }
}
