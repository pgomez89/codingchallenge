﻿namespace CodingChallenge.Data.Classes
{
    public class Idioma
    {
        #region Idiomas

        public const int Castellano = 1;
        public const int Ingles = 2;
        public const int Italiano = 3;

        #endregion

        /// <summary>
        /// Cuando no hay ninguna forma, se devuelve un mensaje de lista vacia, según el idioma indicado
        /// </summary>
        /// <param name="idioma"></param>
        /// <returns></returns>
        public static string ListaVacia(int idioma)
        {
            switch (idioma)
            {
                case Castellano:
                    return "<h1>Lista vacía de formas!</h1>";
                case Ingles:
                    return "<h1>Empty list of shapes!</h1>";
                case Italiano:
                    return "<h1>Elenco vuoto di forme!</h1>";
            }

            return string.Empty;
        }

        /// <summary>
        /// Cabecera del comienzo del reporte, según el idioma indicado
        /// </summary>
        /// <param name="idioma"></param>
        /// <returns></returns>
        public static string Header(int idioma)
        {
            switch (idioma)
            {
                case Castellano:
                    return "<h1>Reporte de Formas</h1>";
                case Ingles:
                    return "<h1>Shapes report</h1>";
                case Italiano:
                    return "<h1>Rapporto di moduli</h1>";
            }

            return string.Empty;
        }

        /// <summary>
        /// Se traducen las formas, según el idioma indicado. En caso de ser más de una forma, la traducción es en plural
        /// </summary>
        /// <param name="idioma"></param>
        /// <param name="cantidad"></param>
        /// <param name="forma"></param>
        /// <returns></returns>
        public static string TraducirForma(int idioma, int cantidad, string forma)
        {
            switch (idioma)
            {
                case Castellano:
                    if(forma == "Circulo")
                        return cantidad == 1 ? "Círculo" : "Círculos";
                    if(forma == "Cuadrado")
                        return cantidad == 1 ? "Cuadrado" : "Cuadrados";
                    if(forma == "TrianguloEquilatero")
                        return cantidad == 1 ? "Triángulo" : "Triángulos";
                    if (forma == "Rectangulo")
                        return cantidad == 1 ? "Rectángulo" : "Rectángulos";
                    if (forma == "TrapecioRectangulo")
                        return cantidad == 1 ? "Trapecio" : "Trapecios";
                    break;
                case Ingles:
                    if (forma == "Circulo")
                        return cantidad == 1 ? "Circle" : "Circles";
                    if (forma == "Cuadrado")
                        return cantidad == 1 ? "Square" : "Squares";
                    if (forma == "TrianguloEquilatero")
                        return cantidad == 1 ? "Triangle" : "Triangles";
                    if (forma == "Rectangulo")
                        return cantidad == 1 ? "Rectangle" : "Rectangles";
                    if (forma == "TrapecioRectangulo")
                        return cantidad == 1 ? "Trapeze" : "Trapezoids";
                    break;
                case Italiano:
                    if (forma == "Circulo")
                        return cantidad == 1 ? "Cerchio" : "Cerchi";
                    if (forma == "Cuadrado")
                        return cantidad == 1 ? "Piazza" : "Piazze";
                    if (forma == "TrianguloEquilatero")
                        return cantidad == 1 ? "Triangolo" : "triangoli";
                    if (forma == "Rectangulo")
                        return cantidad == 1 ? "Rettangolo" : "Rettangoli";
                    if (forma == "TrapecioRectangulo")
                        return cantidad == 1 ? "Trapezio" : "Trapezi";
                    break;
            }

            return string.Empty;
        }

        /// <summary>
        /// Se traduce la palabra "forma", según el idioma indicado
        /// </summary>
        /// <param name="idioma"></param>
        /// <returns></returns>
        public static string TraducirFormaTotal(int idioma)
        {
            switch (idioma)
            {
                case Castellano:
                    return "formas";
                case Ingles:
                    return "shapes";
                case Italiano:
                    return "forme";
            }

            return string.Empty;
        }

        /// <summary>
        /// Se traduce la palabra "area", según el idioma indicado
        /// </summary>
        /// <param name="idioma"></param>
        /// <returns></returns>
        public static string TraducirArea(int idioma)
        {
            switch (idioma)
            {
                case Castellano:
                    return "Área";
                case Ingles:
                    return "Area";
                case Italiano:
                    return "La zona";
            }

            return string.Empty;
        }

        /// <summary>
        /// Se traduce la palabra "perimetro", según el idioma indicado
        /// </summary>
        /// <param name="idioma"></param>
        /// <returns></returns>
        public static string TraducirPerimetro(int idioma)
        {
            switch (idioma)
            {
                case Castellano:
                    return "Perímetro";
                case Ingles:
                    return "Perimeter";
                case Italiano:
                    return "Perimetro";
            }

            return string.Empty;
        }
    }
}
