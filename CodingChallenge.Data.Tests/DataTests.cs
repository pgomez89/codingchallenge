﻿using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                Reporte.Imprimir(new List<FormaGeometrica>(), Idioma.Castellano));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                Reporte.Imprimir(new List<FormaGeometrica>(), Idioma.Ingles));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> { new Cuadrado(5) };

            var resumen = Reporte.Imprimir(cuadrados, Idioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Área 25 | Perímetro 20 <br/>TOTAL:<br/>1 formas Perímetro 20 Área 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3)
            };

            var resumen = Reporte.Imprimir(cuadrados, Idioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = Reporte.Imprimir(formas, Idioma.Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Circles | Area 13,01 | Perimeter 18,06 <br/>2 Squares | Area 29 | Perimeter 28 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = Reporte.Imprimir(formas, Idioma.Castellano);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Círculos | Área 13,01 | Perímetro 18,06 <br/>2 Cuadrados | Área 29 | Perímetro 28 <br/>3 Triángulos | Área 49,64 | Perímetro 51,6 <br/>TOTAL:<br/>7 formas Perímetro 97,66 Área 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConUnTrapecioEnCastellano()
        {
            var trapecio = new List<FormaGeometrica> { new TrapecioRectangulo(5, 3, 4) };

            var resumen = Reporte.Imprimir(trapecio, Idioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Trapecio | Área 16 | Perímetro 16,47 <br/>TOTAL:<br/>1 formas Perímetro 16,47 Área 16", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnTrapecioEnIngles()
        {
            var trapecio = new List<FormaGeometrica> { new TrapecioRectangulo(5, 3, 4) };

            var resumen = Reporte.Imprimir(trapecio, Idioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>1 Trapeze | Area 16 | Perimeter 16,47 <br/>TOTAL:<br/>1 shapes Perimeter 16,47 Area 16", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnTrapecioEnItaliano()
        {
            var trapecio = new List<FormaGeometrica> { new TrapecioRectangulo(5, 3, 4) };

            var resumen = Reporte.Imprimir(trapecio, Idioma.Italiano);

            Assert.AreEqual("<h1>Rapporto di moduli</h1>1 Trapezio | La zona 16 | Perimetro 16,47 <br/>TOTAL:<br/>1 forme Perimetro 16,47 La zona 16", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTrapeciosEnItaliano()
        {
            var trapecio = new List<FormaGeometrica>
            {
                new TrapecioRectangulo(5, 3, 4),
                new TrapecioRectangulo(3, 3, 1),
                new TrapecioRectangulo(6, 3, 3),
                new TrapecioRectangulo(10, 7, 6)
            };

            var resumen = Reporte.Imprimir(trapecio, Idioma.Italiano);

            Assert.AreEqual("<h1>Rapporto di moduli</h1>4 Trapezi | La zona 83,5 | Perimetro 70,42 <br/>TOTAL:<br/>4 forme Perimetro 70,42 La zona 83,5", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnIngles()
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new TrapecioRectangulo(5, 3, 4),
                new Rectangulo(5, 3),
                new TrapecioRectangulo(10, 7, 6),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = Reporte.Imprimir(formas, Idioma.Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>1 Circle | Area 5,94 | Perimeter 8,64 <br/>1 Square | Area 25 | Perimeter 20 <br/>1 Rectangle | Area 15 | Perimeter 16 <br/>2 Trapezoids | Area 67 | Perimeter 46,18 <br/>2 Triangles | Area 42,71 | Perimeter 39,6 <br/>TOTAL:<br/>7 shapes Perimeter 130,42 Area 155,65",
                resumen);
        }
    }
}
